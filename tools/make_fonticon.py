#!/usr/bin/env python

import codecs
import sys
import re


def write_js_out(symbols, file_name):
    js_template_header = """
.pragma library

var map = {
"""

    js_template_footer = """
};
"""

    javascript_out = codecs.open(file_name, encoding='utf-8', mode='w')

    javascript_out.write(js_template_header)

    for name, code in list(symbols):
        javascript_out.write(u"\t'%s' : '\\u%s',\n" % (name, code.strip("\\").upper()))

    javascript_out.write(js_template_footer)
    javascript_out.close()


def get_symbols(file_name):
    contents = open(file_name).read()
    pattern = r'\.(.+):before {\s*content: "(\\[efu][\da-f]+)";\s*}'
    search_obj = re.findall(pattern, contents, re.M | re.I)

    if search_obj:
        print search_obj
    else:
        print "Nothing found!!"

    return search_obj


if __name__ == "__main__":
    if len(sys.argv) < 3:
        input_css = "flaticon.css"
        output_js = "flaticon.js"
    else:
        input_css = sys.argv[1]
        output_js = sys.argv[2]
    symbols = get_symbols(input_css)
    write_js_out(symbols, output_js)

