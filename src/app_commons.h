#ifndef APP_COMMONS_HEADER_GUARD
#define APP_COMMONS_HEADER_GUARD

namespace DB {
class UPDatabase;
}

class DeviceManager;
class DeviceCalendarManager;
class DeviceHardwareService;

class GlobalHandle {
public:
    static DB::UPDatabase *getApplicationDB();
    static DeviceManager* deviceManager();
    static DeviceCalendarManager* deviceCalendarManager();
    static DeviceHardwareService* deviceHardwareService();
};

#endif // APP_COMMONS_HEADER_GUARD

