import QtQuick 2.0
import Material 0.2
import SmartControls 1.0
import QtQuick.Layouts 1.1

Page {
    id: page
    title: "Công tắc thông minh"
    actions: [
        Action {
            name: "Print"

            // Icon name from the Google Material Design icon pack
            iconName: "content/add_circle_outline"
            onTriggered: page.push(scanningDevices_component)
        }
    ]

    DeviceListController {
        id: controller

        onChangeBrightSuccess: {
            console.log("onChangeBrightSuccess.........................")
            hideLoadingView()
            snackbar.open("Thay đổi độ sáng của bóng đèn thành công.");
        }

        onChangeBrightFail: {
            console.log("onChangeBrightFail.........................--")
            hideLoadingView()
            console.info(snackbar)
            snackbar.open("Có lỗi xảy ra trong quá trình thay đổi độ sáng của bóng đèn.")
        }

        onShowMsg: {
            hideLoadingView();
            console.log("onShowMsg.........................message:" + message)
        }

        onStartReadDevicesState: {
            console.log("onShowMsg.........................onReadDevicesStat start")
            showLoadingView();
        }

        onReadDeviceStateFinish: {
            console.log("onShowMsg.........................onReadDeviceState Finish")
            hideLoadingView();
        }

        onReadDeviceStateSuccess: {

        }

        onReadDeviceStateFail: {

        }
    }

    ListView {
        id: deviceListView
        anchors {
            top: parent.top
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        clip: true
        model: controller.deviceList
        delegate: device_component
    }

    Snackbar {
        id: snackbar
    }


    Component {
        id: device_component
        Item {
            width: parent.width
            height: Units.dp(164)

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("go to device detail", deviceId)
//                    page.push(deviceDetail_component, {
//                                        deviceId:deviceId
//                                    })
                    //clickedDeviceItem(deviceAddress, deviceName)
                }
            }

            Icon {
                id: icon
                width: Units.dp(70)
                height: Units.dp(70)
                anchors {
                    left: parent.left
                    leftMargin: Units.dp(10)
                    verticalCenter: parent.verticalCenter
                }
                name: "device/bluetooth"
                color: "#0A3D91"
                size: Units.dp(48)
            }
            Column {
                anchors {
                    top: parent.top
                    topMargin: Units.dp(10)
                    left: icon.right
                    leftMargin: Units.dp(10)
                    right: rightItem.left
                    rightMargin: Units.dp(10)
                    verticalCenter: parent.verticalCenter
                }
                spacing: Units.dp(5)
                Text {
                    text: deviceName
                    font { pixelSize: Units.dp(16); family: "Helvetica" }
                    font.bold: true
                    height: Units.dp(25)
                    width: parent.width
                }
                Row {
                    width: 2*Units.dp(100)
                    height: Units.dp(88)
                    spacing: Units.dp(0)

//                    MouseArea {
//                        anchors.fill: parent
//                        onClicked: {

//                        }
//                    }

                    Rectangle {
                        width: Units.dp(48)
                        height: Units.dp(48)
                        anchors {
                            bottom: parent.bottom
                        }
                        color: "transparent"
                        Icon {
                            id: iconDark
                            width: Units.dp(12)
                            height: Units.dp(12)
                            anchors {
                                centerIn: parent
                            }
                            name: "image/brightness_1"
                            size: Units.dp(12)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                slider.value = 0
                            }
                        }
                    }

                    Slider {
                        id: slider
                        width: Units.dp(200)
                        Layout.alignment: Qt.AlignCenter
                        value: deviceValue
                        tickmarksEnabled: true
                        numericValueLabel: true
                        knobLabel: value + "%"
                        knobDiameter: Units.dp(44)
                        alwaysShowValueLabel:true

                        stepSize: 10
                        minimumValue: 0
                        maximumValue: 100
                        darkBackground: false

                        anchors {
                            bottom: parent.bottom
                            bottomMargin: Units.dp(14)
                        }

                        onValueChanged: {
                            print("From Slider.qml" ,value)
                            print("activeFocus:" ,activeFocus)
                            showLoadingView()
//                            controller.changeBright(deviceAddress, value)
                            controller.addToStack(deviceAddress, value)
                        }
//                        MouseArea {
//                            anchors.fill: parent
//                            propagateComposedEvents: true
//                            onReleased: {
//                                print("MourseArea onReleased$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
//                            }
//                        }

                    }
                    Rectangle {
                        width: Units.dp(48)
                        height: Units.dp(48)
                        anchors {
                            bottom: parent.bottom
                        }
                        color: "transparent"
                        Icon {
                            id: iconBright
                            width: Units.dp(24)
                            height: Units.dp(24)
                            anchors {
                                centerIn: parent
                            }
                            name: "image/wb_sunny"
                            color: Theme.dark.accentColor
                            size: Units.dp(24)
                        }
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                slider.value = 100
                            }
                        }
                    }
                }

                Text {
                    id: alarm
                    text: calendarSummary
                    font { pixelSize: Units.dp(14); family: "Helvetica" }
                    width: parent.width
                    wrapMode: Text.WordWrap
                    verticalAlignment: Text.AlignVCenter
                    height: Units.dp(32)
                }
            }

            Item {
                id: rightItem
                anchors {
                    top: parent.top
                    topMargin: Units.dp(10)
                    right: parent.right
                    rightMargin: Units.dp(10)
                    bottom: parent.bottom
                    bottomMargin: Units.dp(10)
                }
                width: Units.dp(40)

                IconButton {
                    width: Units.dp(40)
                    height: Units.dp(40)
                    anchors {
                        top: parent.top
                    }
                    iconName: "action/schedule"
                    size: Units.dp(32)

                    onClicked: {
                        page.push(deviceCalendar_component, {
                                            deviceId:deviceId
                                        })
                    }
                }

                IconButton {
                    id: editIcon
                    width: Units.dp(40)
                    height: Units.dp(40)
                    anchors {
                        bottom: parent.bottom
                    }
                    iconName: "editor/mode_edit"
                    size: Units.dp(32)
                    onClicked: {
                        page.push(updateDeviceInfo_component, {
                                            _deviceId: deviceId,
                                            _deviceAddress: deviceAddress,
                                            _deviceName:deviceName,
                                            _deviceAvatar:deviceAvatar
                                        })
                    }
                }
            }

            ThinDivider {
                color: "gray"
                anchors.bottom: parent.bottom
            }
        }
    }

    Component {
        id: deviceDetail_component
        DeviceDetailView {

        }
    }

    Component {
        id: deviceCalendar_component
        DeviceCalendarListView {}
    }

    Component {
        id: updateDeviceInfo_component
        UpdateDeviceInfo {
            onSaveDeviceInfo: {
                console.log("onSaveDeviceInfo, _deviceId:" + _deviceId)
                console.log("onSaveDeviceInfo, _deviceAddress:" + _deviceAddress)
                console.log("onSaveDeviceInfo, _deviceName:" + _deviceName)
                console.log("onSaveDeviceInfo, _deviceAvatar:" + _deviceAvatar)
                controller.updateDeviceInfo(_deviceId, _deviceAddress, _deviceName, _deviceAvatar)
            }
        }
    }

    Component {
        id: scanningDevices_component
        DeviceScanningView {

        }
    }
}
