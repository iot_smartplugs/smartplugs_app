#include <QDebug>

#include "app_commons.h"
#include "configuration.h"

#include "zlistdatamodel.h"
#include "datamodel/devicedatamodel.h"

#include "manager/devicemanager.h"
#include "manager/devicecalendarmanager.h"
#include "manager/devicehardwareservice.h"
#include "devicelistcontroller.h"
#include <QDateTime>

DeviceListController::DeviceListController(QObject *parent) :
    currentTime(0),
    currentBright(NULL),
    timeoutReadDeviceState(NULL),
    QObject(parent),
    deviceList_(new ZListDataModel(this)) {
    deviceList_->registerRoleNames(DeviceDataModel::roleNames());

    DeviceManager *deviceManager = GlobalHandle::deviceManager();
    connect(deviceManager,SIGNAL(deviceListChanged()), this, SLOT(onDeviceListChanged()));

    initialize();
    DeviceHardwareService* deviceHardwareService = GlobalHandle::deviceHardwareService();
    connect(deviceHardwareService, &DeviceHardwareService::updateCharacteristicsSuccessful,
            [this](const QString &remoteAddress, const int value) {
//        DeviceManager *deviceManager = GlobalHandle::deviceManager();
//        int state = !deviceManager->getDeviceState(remoteAddress);
//        this->onChangeBrightSuccess(remoteAddress, state);
        this->onChangeBrightSuccess(remoteAddress, value);
        countRetry = 0;
        currentDeviceAddress = "";
    });
    connect(deviceHardwareService, &DeviceHardwareService::updateCharacteristicsFailed,
            [this](const QString &remoteAddress, const int value) {
//        DeviceManager *deviceManager = GlobalHandle::deviceManager();
//        int state = deviceManager->getDeviceState(remoteAddress);
//        this->onChangeBrightFail(remoteAddress, state);
        qDebug() << Q_FUNC_INFO << "#####################################";
        countRetry++;
        currentDeviceAddress = remoteAddress;
        if (countRetry <= 3 && !remoteAddress.isEmpty() && remoteAddress != "") {
            changeBright(remoteAddress, value);
            return;
        } else {
            countRetry = 0;
            currentDeviceAddress = "";
        }
        this->onChangeBrightFail(remoteAddress, value);
    });
    connect(deviceHardwareService, &DeviceHardwareService::result,
            [this](QString &remoteAddress,
            QString &serviceUuid,
            QString &characteristicsUuid,
            QString &value) {
        Q_UNUSED(remoteAddress);
        Q_UNUSED(serviceUuid);
        qDebug()<<"read characteristics: " << characteristicsUuid;
        qDebug()<<"value: "<<value;

    });
    connect(deviceHardwareService, &DeviceHardwareService::result,
            [this](const QString &remoteAddress, const QString &serviceUuid,const QString &characteristicsUuid,const QString &value) {
        this->onReadDeviceStateSuccess(remoteAddress, serviceUuid, characteristicsUuid, value);
    });
    connect(deviceHardwareService, &DeviceHardwareService::readCharacteristicsFailed,
            [this](const QString &remoteAddress) {
        this->onReadDeviceStateFail(remoteAddress);
    });
    isReady = true;
}

QAbstractListModel* DeviceListController::deviceList() const {
    return deviceList_;
}

void DeviceListController::initialize() {

    onDeviceListChanged();
//    readDevicesState();
}

//void DeviceListController::turnPlug(const QString deviceAddress, const bool isTurnOn) {
//    qDebug() << Q_FUNC_INFO << "start, address:" << deviceAddress << ", isTurnOn:" << isTurnOn;

//    DeviceHardwareService* deviceHardwareService = GlobalHandle::deviceHardwareService();
//    int value = Configuration::TurnOnLamp;
//    if (!isTurnOn) {
//        value = Configuration::TurnOffLamp;
//    }
//    //deviceHardwareService->updateCharacteristic(deviceAddress, Configuration::ServiceId, Configuration::CharacteristicId, value);
//    deviceHardwareService->readValue(deviceAddress,Configuration::ServiceId, Configuration::CharacteristicId);
//}

void DeviceListController::changeBright(const QString deviceAddress, const int value) {
    qDebug() << Q_FUNC_INFO << "start, address:" << deviceAddress << ", value:" << value;
    DeviceManager* manager = GlobalHandle::deviceManager();
    int currentValue = manager->getDeviceState(deviceAddress);
//    if (currentValue == value) {
//        qDebug() << Q_FUNC_INFO << "value == current value.";
//        emit showMsg("");
//        return;
//    }
    DeviceHardwareService* deviceHardwareService = GlobalHandle::deviceHardwareService();
    deviceHardwareService->updateCharacteristic(deviceAddress, Configuration::ServiceId, Configuration::CharacteristicId, value);
}

void DeviceListController::onDeviceListChanged() {
    DeviceManager* manager = GlobalHandle::deviceManager();
    DeviceCalendarManager* calendarManager = GlobalHandle::deviceCalendarManager();
    std::list<ZDevice>& devices = manager->getDeviceList();

    QList<ZAbstractRowDataModelPtr> list;
    for (ZDevice& d : devices) {
        if (d.deviceId() <= 0) {
            continue;
        }

        int value = manager->getDeviceState(d.remoteAddress());
        QString calendarSummary = calendarManager->getCalendarSummary(d.deviceId());
        ZAbstractRowDataModelPtr item(new DeviceDataModel(
                    d.deviceId(), d.deviceName(), d.remoteAddress(), value, true, d.deviceAvatar(), calendarSummary));
        list.append(item);
    }

    deviceList_->setListContent(list);
}

//void DeviceListController::onTurnPlugSuccess(const QString address, const bool isTurnOn) {
//    qDebug() << Q_FUNC_INFO << "start, address:" << address << ", isTurnOn:" << isTurnOn;
//    DeviceManager *deviceManager = GlobalHandle::deviceManager();
//    deviceManager->updateDeviceState(address, isTurnOn);
//    emit turnPlugSucess();
//}

//void DeviceListController::onTurnPlugFail(const QString address, const bool isTurnOn) {
//    qDebug() << Q_FUNC_INFO << "start, address:" << address<< ", isTurnOn:" << isTurnOn;
//    emit turnPlugFail();
//}

void DeviceListController::onChangeBrightSuccess(const QString address, const int value) {
    qDebug() << Q_FUNC_INFO << "start, address:" << address << ", value:" << value;
    emit changeBrightSuccess();
    DeviceManager *deviceManager = GlobalHandle::deviceManager();
    deviceManager->updateDeviceState(address, value);
    isReady = true;
    processChangeBright();
}

void DeviceListController::onChangeBrightFail(const QString address, const int value) {
    qDebug() << Q_FUNC_INFO << "start, address:" << address<< ", value:" << value;
    emit changeBrightFail();
    isReady = true;
    processChangeBright();
}

void DeviceListController::addToQueueReadDeviceState(ZDevice device) {
    qDebug() << Q_FUNC_INFO << "start.......";
    queueReadDeviceState.enqueue(new ZDevice(device));
}

void DeviceListController::readDeviceState(const ZDevice *device) {
    qDebug() << Q_FUNC_INFO << "start......";
    if (device->remoteAddress().isNull() || device->remoteAddress() == "") {
        return;
    }
    DeviceHardwareService* deviceHardwareService = GlobalHandle::deviceHardwareService();
    deviceHardwareService->readValue(device->remoteAddress(), Configuration::ServiceId, Configuration::CharacteristicId);
}

void DeviceListController::readDevicesState() {
    DeviceManager* manager = GlobalHandle::deviceManager();
    std::list<ZDevice>& devices = manager->getDeviceList();

    for (ZDevice& d : devices) {
        if (d.deviceId() <= 0) {
            continue;
        }

        //Add device to queueReadDeviceState
        for (std::list<ZDevice>::iterator it = devices.begin(); it != devices.end(); it++) {
            addToQueueReadDeviceState(*it);
        }
    }

    processReadDeviceState();
}

void DeviceListController::processReadDeviceState() {
    qDebug() << Q_FUNC_INFO << "start......";
    if (timeoutReadDeviceState != nullptr) {
        timeoutReadDeviceState->stop();
    }

    if (queueReadDeviceState.isEmpty()) {
        onDeviceListChanged();
        emit readDeviceStateFinish();
        return;
    }
    emit startReadDevicesState();
    if (timeoutReadDeviceState == nullptr) {
        timeoutReadDeviceState = new QTimer(this);
    }

    timeoutReadDeviceState->setInterval(READ_DEVICE_STATE_TIMEOUT);
    timeoutReadDeviceState->setSingleShot(true);
    disconnect(timeoutReadDeviceState, SIGNAL(timeout()), 0, 0);
    connect(timeoutReadDeviceState, SIGNAL(timeout()), SLOT(processReadDeviceState()));
    timeoutReadDeviceState->start();

    ZDevice *device = queueReadDeviceState.dequeue();
    readDeviceState(device);
}

void DeviceListController::onReadDeviceStateSuccess(const QString &remoteAddress, const QString &serviceUuid,const QString &characteristicsUuid,const QString &value) {
    DeviceManager *deviceManager = GlobalHandle::deviceManager();
    deviceManager->updateDeviceState(remoteAddress, value.toInt());
    emit readDeviceStateSuccess(remoteAddress, serviceUuid, characteristicsUuid, value);
    processReadDeviceState();
}

void DeviceListController::onReadDeviceStateFail(const QString deviceAddress) {
    emit readDeviceStateFail(deviceAddress);
    processReadDeviceState();
}

void DeviceListController::updateDeviceInfo(const int deviceId, const QString deviceAddress, const QString deviceName, const QString deviceAvatar) {
    qDebug() << Q_FUNC_INFO << "start, deviceId:" << deviceId;
    qDebug() << Q_FUNC_INFO << "start, deviceAddress:" << deviceAddress;
    qDebug() << Q_FUNC_INFO << "start, deviceName:" << deviceName;
    qDebug() << Q_FUNC_INFO << "start, deviceAvatar:" << deviceAvatar;
    DeviceManager* manager = GlobalHandle::deviceManager();
    manager->updateDeviceData(deviceId, deviceAddress, deviceName, deviceAvatar);
}

void DeviceListController::processChangeBright() {
    qDebug() << Q_FUNC_INFO;
    if (!isReady) {
        return;
    }
    QDateTime time = QDateTime::currentDateTime();
    const uint newTime= time.toMSecsSinceEpoch();
    if (newTime - currentTime < CHANGE_BRIGHT_TIME) {
        return;
    }
    if (currentBright == NULL) {
        //timeoutHelper->stop();
        return;
    }
//    mutex.lock();
//    ZQueueBright *lampBright = stackLampBright.pop();
//    stackLampBright.clear();
//    currentBright = NULL;
//    mutex.unlock();
    currentTime = newTime;
    isReady = false;
    changeBright(currentBright->getDeviceAddress(), currentBright->getPercentBright());
    currentBright = NULL;
}

void DeviceListController::addToStack(const QString deviceAddress, const int percentBright) {
//    qDebug() << Q_FUNC_INFO;
//    if (!timeoutHelper) {
//        timeoutHelper = new QTimer(this);
//        qDebug() << Q_FUNC_INFO << "????????????";
//        disconnect(timeoutHelper, SIGNAL(timeout()), 0, 0);
//        connect(timeoutHelper, SIGNAL(timeout()), this, SLOT(processChangeBright()));
//        timeoutHelper->setInterval(100);
//    }

//    qDebug() << Q_FUNC_INFO << "1";
//    if (!timeoutHelper->isActive()) {
//        timeoutHelper->start(100);
//    }
    qDebug() << Q_FUNC_INFO << "percentBright:" << percentBright;
//    ZQueueBright *lampBright= new ZQueueBright();
//    lampBright->setDeviceAddress(deviceAddress);
//    lampBright->setPercentBright(percentBright);
//    mutex.lock();
//    if (currentBright != NULL && currentBright->getDeviceAddress() == deviceAddress && currentBright->getPercentBright() == percentBright) {
//        return;
//    }
    currentBright = new ZQueueBright();
    currentBright->setDeviceAddress(deviceAddress);
    currentBright->setPercentBright(percentBright);
//    mutex.unlock();
    processChangeBright();
}

