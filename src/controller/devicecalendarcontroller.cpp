#include <QDebug>
#include <zlistdatamodel.h>

#include "app_commons.h"
#include "datamodel/devicecalendardatamodel.h"

#include "manager/devicemanager.h"
#include "manager/devicecalendarmanager.h"
#include "model/zdevice.h"
#include "model/zdevicecalendaritem.h"

#include "devicecalendarcontroller.h"

DeviceCalendarController::DeviceCalendarController(QObject *parent) : QObject(parent)
{
    calendarList_ = new ZListDataModel(this);
    calendarList_->registerRoleNames(DeviceCalendarDataModel::roleNames());
}

void DeviceCalendarController::removeCalendar(int32_t calendarId) {

}

void DeviceCalendarController::updateCalendar() {

}

void DeviceCalendarController::addCalendar() {

}

int32_t DeviceCalendarController::deviceId() const {
    return deviceId_;
}

QString DeviceCalendarController::deviceName() const {
    return deviceName_;
}

QString DeviceCalendarController::deviceAvatar() const {
    return deviceAvatar_;
}

int32_t DeviceCalendarController::deviceState() const {
    return deviceState_;
}

void DeviceCalendarController::setDeviceId(int32_t value) {
    if (deviceId_ == value) {
        return;
    }

    // get device calendar
    deviceId_ = value;
    DeviceManager* manager = GlobalHandle::deviceManager();
    ZDevice d = manager->getDevice(deviceId_);
    deviceAvatar_ = d.deviceAvatar();
    deviceName_ = d.deviceName();
    deviceState_ = manager->getDeviceState(d.remoteAddress())?1:0;

    emit deviceInfoUpdated();

    initializeCalendarList();
}

QAbstractListModel* DeviceCalendarController::calendarList() const {
    return calendarList_;
}

void DeviceCalendarController::initializeCalendarList() {
    DeviceCalendarManager* manager = GlobalHandle::deviceCalendarManager();
    ZDeviceCalendarList deviceCalendarItems = manager->getCalendarItemList(deviceId_);
    QList<ZAbstractRowDataModelPtr> contents;
    for (int i = 0; i < deviceCalendarItems.size(); ++i) {
        ZDeviceCalendarItem deviceCalendarItem = deviceCalendarItems.at(i);
        DeviceCalendarDataModel *dataModel = new DeviceCalendarDataModel(deviceCalendarItem.deviceId_,
                                                                         deviceCalendarItem.calendarId_,
                                                                         deviceCalendarItem.autoOn_,
                                                                         deviceCalendarItem.hour_,
                                                                         deviceCalendarItem.minute_,
                                                                         deviceCalendarItem.repeartType_,
                                                                         deviceCalendarItem.repeartItems_
                                                            );
        contents.append(ZAbstractRowDataModelPtr(dataModel));
    }
    calendarList_->setListContent(contents);
}

void DeviceCalendarController::deleteDeviceCalendar(int row, int calendarId) {
    calendarList_->removeItem(row);
    DeviceCalendarManager* manager = GlobalHandle::deviceCalendarManager();
    manager->removeDeviceCalendar(calendarId);
}

void DeviceCalendarController::updateDeviceCalendar(int calendarId,
                                                    int hour,
                                                    int minute,
                                                    int repeartMode,
                                                    bool autoOn)
{
    qDebug()<<"______CPP_______";
    qDebug()<<"calendarId: " << calendarId;
    qDebug()<<"hour: " << hour;
    qDebug()<<"minute: "<< minute;
    qDebug()<<"repeartMode: " << repeartMode;
    qDebug()<<"autoOn: "<<autoOn;

    ZDeviceCalendarItem calendarItem;
    calendarItem.setCalendarId(calendarId);
    calendarItem.setDeviceId(deviceId_);
    calendarItem.setHour(hour);
    calendarItem.setMinute(minute);
    calendarItem.setRepeartType(static_cast<ZDeviceCalendarItem::RepeartType>(repeartMode));
    calendarItem.autoOn_ = autoOn;
    DeviceCalendarManager* manager = GlobalHandle::deviceCalendarManager();
    manager->updateDeviceCalendar(deviceId_,calendarItem);
    initializeCalendarList();
}
