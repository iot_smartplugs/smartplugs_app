#include "devicehardwareservice.h"
#include <QTimer>
#include <QDebug>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QLowEnergyController>
#include <QLowEnergyService>
#include <QBluetoothServiceInfo>
#include <QBluetoothServiceDiscoveryAgent>
#include "configuration.h"
#include <QTextCodec>

class DeviceHardwareService::DeviceHardwareServicePrivate {
public:
    DeviceHardwareServicePrivate(DeviceHardwareService &object) :
        q_ptr(&object),
        m_controller(NULL)
    {
        qDebug()<<"------------- DeviceHardwareServicePrivate";
    }
    ~DeviceHardwareServicePrivate()
    {

    }

    void connectToDevice(const QString &remoteAddress)
    {
        QBluetoothDeviceInfo * bluetoothDeviceInfo;
    #ifdef Q_OS_MAC
        QBluetoothUuid uuid(remoteAddress);
        bluetoothDeviceInfo = new QBluetoothDeviceInfo(uuid,"",0);
    #else
        QBluetoothAddress address(remoteAddress);
        bluetoothDeviceInfo = new QBluetoothDeviceInfo(address,"",0);
    #endif
        m_controller = new QLowEnergyController(*bluetoothDeviceInfo);
        QObject::connect(m_controller, &QLowEnergyController::connected,
                         [this](){
            qDebug()<<"connect to device";
            QString serviceUuid = this->updateData["serviceUuid"].toString();
            QString characteristicsUuid = this->updateData["characteristicsUuid"].toString();
            int value = this->updateData["value"].toInt();
            this->updateCharacteristic(serviceUuid,
                                       characteristicsUuid,
                                       value);
        });
        QObject::connect(m_controller, &QLowEnergyController::serviceDiscovered,
                         [this](const QBluetoothUuid &newService) {
            qDebug() << "serviceDiscovered: "<< newService.toString();
            QString serviceUuid = this->updateData["serviceUuid"].toString();
            if (newService == QBluetoothUuid(serviceUuid)) {
                this->updateCharacteristic();
            }
        });
        QObject::connect(m_controller, &QLowEnergyController::discoveryFinished,
                         [this](){

        });
        m_controller->connectToDevice();
    }

    void updateCharacteristic() {
        QString serviceUuid = this->updateData["serviceUuid"].toString();
        QString characteristicsUuid = this->updateData["characteristicsUuid"].toString();
        int value = this->updateData["value"].toInt();

        this->updateCharacteristic(serviceUuid,
                                   characteristicsUuid,
                                   value);
    }

    void updateCharacteristic(const QString &serviceUuid,
                              const QString &characteristicsUuid,
                              const int value)
    {
        qDebug()<<"serviceUuid: " << serviceUuid;
        qDebug()<<"characteristicsUuid: " << characteristicsUuid;
        qDebug()<<"value: " << value;
        QBluetoothUuid serviceUuid_(serviceUuid);
        qDebug()<<"------------- Step 2";

        QLowEnergyService *service = m_controller->createServiceObject(serviceUuid_);
        qDebug()<<"------------- Step 3";
        if (!service) {
            qDebug()<<"m_controller->discoverServices";
            m_controller->discoverServices();
            return;
        }
        const QLowEnergyCharacteristic& energyCharacter = service->characteristic(QBluetoothUuid(characteristicsUuid));
        qDebug()<<"------------- Step 4";
        if(!energyCharacter.isValid())
        {
            if (service->state() == QLowEnergyService::DiscoveryRequired) {
                //! [les-service-3]
                QObject::connect(service, &QLowEnergyService::stateChanged,
                                 [this](QLowEnergyService::ServiceState newState){
                    qDebug()<<"stateChanged";
                    if (newState != QLowEnergyService::ServiceDiscovered) {
                        if (newState != QLowEnergyService::DiscoveringServices) {
                            qDebug()<<"newState != QLowEnergyService::DiscoveringServices";
                        }
                        return;
                    }
                    this->updateCharacteristic();
                });
                service->discoverDetails();
                return;
            }
            qDebug() << "Device is not really valid";
            qDebug() << "is tx valid? " << energyCharacter.isValid();
            qDebug() << "These are the characteristic uuids I see: ";
            Q_FOREACH(auto c, service->characteristics())
            {
                qDebug() << "uuid:" << c.uuid().toString();
            }
            return;
        }
        Q_Q(DeviceHardwareService);
        connect(service, &QLowEnergyService::characteristicWritten,
                [this,q](const QLowEnergyCharacteristic &info, const QByteArray &value){
            for (int i= 0; i < value.size(); i++) {
                qDebug() << "index:" << i << "; value:" << value[i];
            }
            qDebug() << "value.toInt():" << value.toInt();
            int currentValue = (int)value[0];
            qDebug() << "currentValue:" << currentValue;
            QString remoteAddress = this->updateData["remoteAddress"].toString();
            q->updateCharacteristicsSuccessful(remoteAddress, value.toInt());
        });
        connect(service, SIGNAL(error(QLowEnergyService::ServiceError)),
                q, SLOT(error(QLowEnergyService::ServiceError)));
//        connect(service, &QLowEnergyService::characteristicWritten,
//                q, &DeviceHardwareService::characteristicWritten);

        qDebug() << Q_FUNC_INFO << "@@@@@@@@@@@@@@@@@@@@@@@@@@@ value= " << value;
        unsigned char byte[1];
        byte[0] = value & 0xFF;
        const char* tmpByteArr= reinterpret_cast<const char*>(byte);
        QByteArray byteArr(tmpByteArr, 1);
        service->writeCharacteristic(energyCharacter, byteArr, QLowEnergyService::WriteWithResponse);
    }

    void readCharacteristic(const QString &serviceUuid,
                              const QString &characteristicsUuid)
    {
        qDebug()<<"serviceUuid: " << serviceUuid;
        qDebug()<<"characteristicsUuid: " << characteristicsUuid;
        QBluetoothUuid serviceUuid_(serviceUuid);
        QLowEnergyService *service = m_controller->createServiceObject(serviceUuid_);
        if (!service) {
            qDebug()<<"m_controller->discoverServices";
            m_controller->discoverServices();
            return;
        }
        const QLowEnergyCharacteristic& energyCharacter = service->characteristic(QBluetoothUuid(characteristicsUuid));
        qDebug()<<"------------- Step 4" << "energyCharacter.isValid():" << energyCharacter.isValid();
        if(!energyCharacter.isValid())
        {
            qDebug()<<"------------- Step 4 222222";
            if (service->state() == QLowEnergyService::DiscoveryRequired) {
                //! [les-service-3]
                qDebug()<<"------------- Step 4 3333333";
                QObject::connect(service, &QLowEnergyService::stateChanged,
                                 [this](QLowEnergyService::ServiceState newState){
                    qDebug()<<"stateChanged";
                    if (newState != QLowEnergyService::ServiceDiscovered) {
                        if (newState != QLowEnergyService::DiscoveringServices) {
                            qDebug()<<"newState != QLowEnergyService::DiscoveringServices";
                        }
                        return;
                    }
                    this->updateCharacteristic();
                });
                service->discoverDetails();
                return;
            }
            qDebug() << "Device is not really valid";
            qDebug() << "is tx valid? " << energyCharacter.isValid();
            qDebug() << "These are the characteristic uuids I see: ";
            Q_FOREACH(auto c, service->characteristics())
            {
                qDebug() << "uuid:" << c.uuid().toString();
            }
            return;
        }
        Q_Q(DeviceHardwareService);
//        connect(service, &QLowEnergyService::characteristicWritten,
//                [this,q](const QLowEnergyCharacteristic &info, const QByteArray &value){
//            QString remoteAddress = this->updateData["remoteAddress"].toString();
//            q->updateCharacteristicsSuccessful(remoteAddress);
//        });
        connect(service, SIGNAL(error(QLowEnergyService::ServiceError)),
                q, SLOT(error(QLowEnergyService::ServiceError)));
        connect(service, &QLowEnergyService::characteristicRead,
                q, &DeviceHardwareService::characteristicRead);

        service->readCharacteristic(energyCharacter);
    }

public slots:
public:
    Q_DECLARE_PUBLIC(DeviceHardwareService)
    DeviceHardwareService *const q_ptr;
    QLowEnergyController *m_controller;
    QVariantMap updateData;
    QTimer timeoutHelper;
};

DeviceHardwareService::DeviceHardwareService(QObject *parent):
    QObject(parent),
    d_ptr(new DeviceHardwareServicePrivate(*this))
{
}

void DeviceHardwareService::updateCharacteristic(QString &remoteAddress,
                                                 QString &serviceUuid,
                                                 QString &characteristicsUuid,
                                                 QString &value)
{
    qDebug() << Q_FUNC_INFO;

}


void DeviceHardwareService::updateCharacteristic(const QString &remoteAddress,
                                                 const QString &serviceUuid,
                                                 const QString &characteristicsUuid,
                                                 const int value)
{

    qDebug() << Q_FUNC_INFO;
    Q_D(DeviceHardwareService);
    if (d->updateData["remoteAddress"].toString() != remoteAddress) {
        if (d->m_controller != NULL) {
            delete d->m_controller;
            d->m_controller = NULL;
        }
    }
    qDebug()<<"----------------- Step 1";
    d->updateData["remoteAddress"] = QVariant::fromValue(remoteAddress);
    d->updateData["serviceUuid"] = QVariant::fromValue(serviceUuid);
    d->updateData["characteristicsUuid"] = QVariant::fromValue(characteristicsUuid);
    d->updateData["value"] = QVariant::fromValue(value);
    qDebug()<<"----------------- Step 2";

    d->timeoutHelper.setInterval(UPDATE_DEVICE_CHARACTERISTICS_TIMEOUT);

    disconnect(&d->timeoutHelper, SIGNAL(timeout()), 0, 0);

    connect(&d->timeoutHelper, &QTimer::timeout,
            [this, d, remoteAddress, value](){
        qDebug()<<"----------------- TIMEOUT";
        d->timeoutHelper.stop();

        if (d->m_controller) {
            if (d->m_controller->state() != QLowEnergyController::UnconnectedState && d->m_controller->state() != QLowEnergyController::ClosingState) {
                d->m_controller->disconnectFromDevice();
            }
            delete d->m_controller;
            d->m_controller = NULL;
        }
        QString abc = QString(remoteAddress.data());
        this->updateCharacteristicsFailed(abc, value);

    });
    qDebug()<<"----------------- Step 3";
    d->timeoutHelper.start();


    qDebug()<<"----------------- Step 4";
    if (d->m_controller == NULL) {
        d->connectToDevice(remoteAddress);
        return;
    }
    qDebug()<<"----------------- Step 5";
    if (d->m_controller->state() == QLowEnergyController::UnconnectedState) {
        d->connectToDevice(remoteAddress);
        return;
    }
    if (d->m_controller->state() == QLowEnergyController::ClosingState) {

    }
    d->updateCharacteristic(serviceUuid,
                            characteristicsUuid,
                            value);

}

void DeviceHardwareService::readValue(const QString &remoteAddress,
                                      const QString &serviceUuid,
                                      const QString &characteristicsUuid)
{
    qDebug() << Q_FUNC_INFO;
    Q_D(DeviceHardwareService);
    if (d->updateData["remoteAddress"].toString() != remoteAddress) {
        if (d->m_controller != NULL) {
            delete d->m_controller;
            d->m_controller = NULL;
        }
    }
    d->updateData["remoteAddress"] = QVariant::fromValue(remoteAddress);
    d->updateData["serviceUuid"] = QVariant::fromValue(serviceUuid);
    d->updateData["characteristicsUuid"] = QVariant::fromValue(characteristicsUuid);
    d->timeoutHelper.setInterval(READ_DEVICE_CHARACTERISTICS_TIMEOUT);

    disconnect(&d->timeoutHelper, SIGNAL(timeout()), 0, 0);

    connect(&d->timeoutHelper, &QTimer::timeout,
            [this, d, remoteAddress](){
        qDebug()<<"----------------- TIMEOUT";
        d->timeoutHelper.stop();

        if (d->m_controller) {
            if (d->m_controller->state() != QLowEnergyController::UnconnectedState && d->m_controller->state() != QLowEnergyController::ClosingState) {
                d->m_controller->disconnectFromDevice();
            }
            delete d->m_controller;
            d->m_controller = NULL;
        }
        QString abc = QString(remoteAddress.data());
        this->readCharacteristicsFailed(abc);

    });
    d->timeoutHelper.start();
    if (d->m_controller == NULL) {
        d->connectToDevice(remoteAddress);
        return;
    }
    if (d->m_controller->state() == QLowEnergyController::UnconnectedState) {
        d->connectToDevice(remoteAddress);
        return;
    }
    if (d->m_controller->state() == QLowEnergyController::ClosingState) {

    }
    d->readCharacteristic(serviceUuid, characteristicsUuid);

}

void DeviceHardwareService::error(QLowEnergyService::ServiceError error)
{
    Q_D(DeviceHardwareService);
    QString remoteAddress = d->updateData["remoteAddress"].toString();
    this->readCharacteristicsFailed(remoteAddress);
}

void DeviceHardwareService::characteristicRead(const QLowEnergyCharacteristic &info,
                                               const QByteArray &value)
{
    Q_D(DeviceHardwareService);
    QString remoteAddress = d->updateData["remoteAddress"].toString();
    QString serviceUuid = d->updateData["serviceUuid"].toString();
    QString characteristicsUuid = d->updateData["characteristicsUuid"].toString();
    QString valueStr = QString::number(value.toInt());
    emit result(remoteAddress,
                serviceUuid,
                characteristicsUuid,
                valueStr);
}

void DeviceHardwareService::characteristicWritten(const QLowEnergyCharacteristic &info,
                                                  const QByteArray &value)
{
    Q_D(DeviceHardwareService);
    QString remoteAddress = d->updateData["remoteAddress"].toString();
    this->updateCharacteristicsSuccessful(remoteAddress, value.toInt());
    if (d->timeoutHelper.isActive()){
        d->timeoutHelper.stop();
    }
}

void DeviceHardwareService::stateChanged(QLowEnergyService::ServiceState newState)
{
    Q_UNUSED(newState);
}
