#ifndef DEVICEHARDWARESERVICE_H
#define DEVICEHARDWARESERVICE_H
#include <QObject>
#include <QSharedPointer>
#include <functional>
#include <QLowEnergyController>
#include <QBluetoothServiceInfo>
#include <QBluetoothServiceDiscoveryAgent>
#include <QTimer>

class DeviceHardwareService : public QObject
{
    Q_OBJECT
public:
    DeviceHardwareService(QObject *parent = 0);
    void updateCharacteristic(QString &remoteAddress,
                          QString &serviceUuid,
                          QString &characteristicsUuid,
                          QString &value);

    void updateCharacteristic(const QString &remoteAddress,
                          const QString &serviceUuid,
                          const QString &characteristicsUuid,
                          const int value);
    void readValue(const QString &remoteAddress,
                   const QString &serviceUuid,
                   const QString &characteristicsUuid);

signals:
    void updateCharacteristicsSuccessful(const QString &remoteAddress, const int value);
    void updateCharacteristicsFailed(const QString &remoteAddress, const int value);

    void readCharacteristicsSuccessful(const QString &remoteAddress);
    void readCharacteristicsFailed(const QString &remoteAddress);

    void result(QString &remoteAddress,
                QString &serviceUuid,
                QString &characteristicsUuid,
                QString &value);

private slots:
    void error(QLowEnergyService::ServiceError);
    void stateChanged(QLowEnergyService::ServiceState newState);
    void characteristicRead(const QLowEnergyCharacteristic &info,
                            const QByteArray &value);

    void characteristicWritten(const QLowEnergyCharacteristic &info,
                               const QByteArray &value);

private:
    const int READ_DEVICE_CHARACTERISTICS_TIMEOUT = 10000;
    const int UPDATE_DEVICE_CHARACTERISTICS_TIMEOUT = 10000;
    class DeviceHardwareServicePrivate;
    QSharedPointer<DeviceHardwareServicePrivate> d_ptr;
    Q_DECLARE_PRIVATE(DeviceHardwareService)

};

#endif // DEVICEHARDWARESERVICE_H
